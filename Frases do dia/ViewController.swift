//
//  ViewController.swift
//  Frases do dia
//
//  Created by Anna Carolina on 27/05/19.
//  Copyright © 2019 Anna Carolina. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var resultadoLegenda: UILabel!
    
    @IBAction func gerarFrases(_ sender: Any) {
        
        var frases: [String] = []
        
        frases = ["Não procure ser o melhor, mas sim o mais simples. Porque até a maior árvore da floresta começa do chão."]
        frases.append("Se você cansar, aprenda a descansar e não a desistir.")
        frases.append("Troque suas folhas, mas não perca suas raízes. Mude suas opiniões, mas não perca seus princípios.")
        frases.append("A paciência é a chave para todos os problemas que não dependem de você.")
        frases.append("Não faça da sua vida um rascunho, poderá não ter tempo de passá-la a limpo.")
        frases.append("Reaja com inteligência mesmo quando for tratado com ignorância.")
        frases.append("Inteligente não é quem sabe para onde ir, mas quem aprendeu para onde não deve voltar.")
        frases.append("Saber esperar é uma virtude. Aceitar, sem questionar, que cada coisa tem um tempo certo para acontecer, é ter fé!")
        
        let numeroAleatorio = arc4random_uniform(8)
        
        resultadoLegenda.text = frases[Int (numeroAleatorio)]
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }


}

